import glob
import os.path
from pathlib import Path

import numpy as np
import pandas as pd
import re
import xarray as xr
from scipy import interpolate

latitude_col_name = 'Station Meteo Latitude'
longitude_col_name = 'Station Meteo Longitude'

def dms2dd(s):
    """Convert string to lat lon decimal value"""
    # example: s = """43;10.3429606N"""
    # example: s = """5;36.4987202E"""
    if type(s) is not str and np.isnan(s):
        return np.nan
    try:
        degrees, minutes_seconds_direction = re.split('[;°\'"]+', s)
        direction = re.sub("[^S^N^W^E]", "", minutes_seconds_direction)
        minutes_seconds = re.sub("[^\d\.]","",minutes_seconds_direction)
        dd = float(degrees) + float(minutes_seconds)/60
        if direction in ('S','W'):
            dd*= -1
    except Exception as e:
        print(f"error procesing line {s}")
        raise e
    return dd


def parse_dir(file_patterns):
    """parse directory, convert values to panda
    and save as csv"""
    for f in glob.glob(file_patterns):

        try:
            df = pd.read_csv(f)
            print(f"processing file {f} ")
            df[latitude_col_name] = df[latitude_col_name].apply(dms2dd)
            df[longitude_col_name] = df[longitude_col_name].apply(dms2dd)
            df.to_csv(f.replace('.txt',"_geoproc.csv"))
        except:
            print(f"error processing file {f}")


#parse_dir(r"\\meskl2\traitements\traitement_Campagnes\DELMOGE\Station Météo\Daily_Log\*.txt")


def read_navigation(input_dir: Path,file_pattern:str,output_file:str):
    """Read all navigation values and merge all in one"""
    df_list= []
    v = input_dir / file_pattern
    for f in glob.glob(str(v)):
            print(f"processing file {f}")
            df = pd.read_csv(f,parse_dates={'datetime': ['Date', 'Time']},infer_datetime_format=True)
            df = df[~df[latitude_col_name].isnull()]
            df_list.append(df)




    print("Merging all dataset")
    #now merge all files
    final_df=pd.concat(df_list)
    merged_csv = str(input_dir /output_file)
    print(f"Saving merged dataset to {merged_csv} ")
    final_df.to_csv(merged_csv)

def convert_csv_to_xarray(input_dir: Path,input_csv:str,output_file:str):
    """Convert csv to xarray dataset"""
    print("Converting to xarray")
    csv_file = str(input_dir / input_csv)
    df = pd.read_csv(csv_file, parse_dates=['datetime'], infer_datetime_format=True)
    xr= df.to_xarray()
    os.chdir(input_dir)
    print(f"Saving as netcdf to {output_file}")
    xr.to_netcdf(output_file)


def interpol_values(input_dir:Path,xr_nc_file:str, time_values):
    base_dataset=xr.load_dataset(str(input_dir / xr_nc_file))
    #build a time
    # We need to change time to float in order to be able to interpolate data
    # We substract the lowest date as a reference date
    times = base_dataset.datetime.data
    latitudes = base_dataset[latitude_col_name].data
    longitudes = base_dataset[longitude_col_name].data

    reference_date = times[0]
    times_float = (times - reference_date) / np.timedelta64(1, "s")
    time_out_float = (time_values - reference_date) / np.timedelta64(1, "s")
    interpolate_function_latitudes = interpolate.interp1d(times_float, (latitudes, times_float), kind='nearest',)
    interpolate_function_lon = interpolate.interp1d(times_float, (longitudes, times_float), kind='nearest')
    (interpolated_latitudes, time_refs) = interpolate_function_latitudes(time_out_float)
    (interpolated_longitudes, time_refs) = interpolate_function_lon(time_out_float)
    return interpolated_longitudes,interpolated_latitudes


def load_detection(input_dir:Path,input_file:str):
    """Load Marie Ponchart detection file"""
    file = input_dir / input_file
    df = pd.read_csv(file, parse_dates={'datetime': ['Date', 'Time']}, infer_datetime_format=True)
    return df

input_dir = Path(r"\\meskl2\traitements\traitement_Campagnes\DELMOGE\Station Meteo\Daily_Log")
merged_csv = "merged_values.csv"

#read_navigation(input_dir=input_dir,file_pattern="2023*.csv",output_file=merged_csv)

xr_file = "merged_values.nc"
#convert_csv_to_xarray(input_dir=input_dir,input_csv=merged_csv,output_file=xr_file )

#really interpol data
input_detection_dir =Path(r"\\meskl2\traitements\traitement_Campagnes\DELMOGE\HYDROPHONE\Traitement")
input_ds= load_detection(input_dir=input_detection_dir,input_file="detections_dauphins.csv")
#make geolocalisation
longitudes,latitudes=interpol_values(input_dir=input_dir,xr_nc_file=xr_file,time_values=input_ds["datetime"].values)

input_ds["longitudes"]=longitudes
input_ds["latitudes"]=latitudes
#save data as csv
input_ds.to_csv(str(input_detection_dir / "geoloc_detection_dauphins.csv"))


print("Done")
